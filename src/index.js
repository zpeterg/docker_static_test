const express = require('express')
const settings = require('./settings')
const  cors = require('cors')

const app = express()
app.use(cors());

const name = 'ServerApp'

app.get('/', function (req, res, next) {
  res.send(`Health is ok on ${name}!`);
});

app.listen(settings.port, () => console.log(`${name} listening on port ${settings.port}!`))
FROM node:10

RUN apt-get update && apt-get install -y \
  curl \
  dnsutils \
  vim \
  net-tools

ARG root_path=/code

# Create app directory (index.js with Express server)
RUN mkdir -p ${root_path}
# Create app static directory (files created by React-static)
WORKDIR ${root_path}

# Install app dependencies - for react-static app or whatever is there
COPY package.json .
RUN npm install

# Now copy-in source files (after above so Yarn isn't run every time)
COPY /src .

CMD node index.js
